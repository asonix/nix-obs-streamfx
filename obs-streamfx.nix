{ lib
, stdenv
, fetchFromGitHub
, cmake
, ffmpeg_4
, ninja
, pkg-config
, qtbase
, obs-studio
}:

stdenv.mkDerivation rec {
  pname = "obs-streamfx";
  version = "0.12.0b299";

  src = fetchFromGitHub {
    owner = "Xaymar";
    repo = "obs-StreamFx";
    rev = version;
    sha256 = "H93wwLiqzkKssxH4PI8OJltjVUlHwv8BKiS7S7rC1UI=";
    fetchSubmodules = true;
  };

  nativeBuildInputs = [ cmake ninja pkg-config ];
  buildInputs = [ ffmpeg_4 obs-studio qtbase ];

  dontWrapQtApps = true;

  cmakeFlags = with lib; [
    "-DCMAKE_BUILD_TYPE=Release"
    "-DENABLE_CLANG=true"
    "-DENABLE_PROFILING=off"
    "-DPACKAGE_NAME=${pname}-${version}"
    "-DVERSION=${version}"
    "-DSTRUCTURE_PACKAGEMANAGER=true"
    "-DSTANDALONE=true"
    "-DENABLE_UPDATER=false"
    "-DENABLE_FRONTEND=true"
    "-DREQUIRE_JSON=true"
  ];

  patches = [ ./parse-version.patch ];

  meta = with lib; {
    description = "StreamFX is a plugin for OBS® Studio which adds many new effects, filters, sources, transitions and encoders - all for free! Be it 3D Transform, Blur, complex Masking, or even custom shaders, you'll find it all here.";
    homepage = "https://s.xaymar.com/streamfx-dc";
    maintainers = [ ];
    license = licenses.gpl2Plus;
    platforms = [ "x86_64-linux" ];
  };
}
