{
  description = "obs-streamfx packaged";

  inputs = {
    nixpkgs.url = github:NixOS/nixpkgs/nixos-23.11;
    flake-utils.url = github:numtide/flake-utils;
  };

  outputs = { nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system: 
      let
        pkgs = import nixpkgs {
          inherit system;
        };
      in
      {
        packages = rec {
          obs-streamfx = pkgs.qt6Packages.callPackage ./obs-streamfx.nix { };
          default = obs-streamfx;
        };
      });
}
