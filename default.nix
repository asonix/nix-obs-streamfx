let
  pkgs = import <nixpkgs> { };
in
with pkgs; {
  obs-streamfx = qt6Packages.callPackage ./obs-streamfx.nix { };
}
